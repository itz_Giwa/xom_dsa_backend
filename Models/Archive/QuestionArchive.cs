﻿
using dsa_backend.Models.Posts;
using Piranha.AttributeBuilder;
using Piranha.Models;

namespace dsa_backend.Models.Archive
{
    [PageType(Title = "Question archive", UseBlocks = false)]
    [PageTypeArchiveItem(typeof(QuestionPost))]
    public class QuestionArchive : ArchivePage<QuestionArchive>
    {
        /// <summary>
        /// Gets/sets the archive hero.
        /// </summary>

    }
}
