﻿using dsa_backend.Models.Posts;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Archive
{
    [PageType(Title = "Events archive", UseBlocks = false)]
    [PageTypeArchiveItem(typeof(EventPost))]
    public class EventsArchive : ArchivePage<EventsArchive>
    {

    }

}
