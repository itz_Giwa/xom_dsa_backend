﻿using dsa_backend.Models.Posts;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Archive
{
    [PageType(Title = "Interests archive", UseBlocks = false)]
    [PageTypeArchiveItem(typeof(InterestPost))]
    public class InterestsArchive : ArchivePage<InterestsArchive>
    {

    }
}
