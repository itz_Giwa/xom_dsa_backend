﻿using dsa_backend.Models.Posts;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Archive
{
    [PageType(Title = "Options archive", UseBlocks = false)]
    [PageTypeArchiveItem(typeof(OptionPost))]
    public class OptionsArchive : ArchivePage<OptionsArchive>
    {
    }
}
