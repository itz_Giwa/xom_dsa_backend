﻿using dsa_backend.Models.Posts;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models
{
    [PageType(Title = "Contacts archive", UseBlocks = false)]
    [PageTypeArchiveItem(typeof(ContactPost))]
    public class ContactsArchive : ArchivePage<ContactsArchive>
    {

    }
}
