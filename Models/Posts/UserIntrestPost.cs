﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{

    [PostType(Title = "User Interest post", UseBlocks = false)]
    public class UserIntrestPost : Post<UserIntrestPost>
    {
        [Region(Title = "User Interest")]
        public UserInterest UserInterest { get; set; }
    }
}
