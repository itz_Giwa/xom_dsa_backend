﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Question post", UseBlocks = false)]
    public class QuestionPost : Post<QuestionPost>
    {
        [Region(Title = "Question")]
        public Question Question { get; set; }
    }
}
