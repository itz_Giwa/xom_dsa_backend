﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Option post", UseBlocks = false)]
    public class OptionPost : Post<OptionPost>
    {
        [Region(Title = "Option")]
        public Option Option { get; set; }
    }
}
