﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "User Resource Favourite post", UseBlocks = false)]
    public class UserResourceFavouritePost : Post<UserResourceFavouritePost>
    {
        [Region(Title = "User  Resource Favourite")]
        public UserResourceFavourite UserResourceFavourite { get; set; }
    }
}
