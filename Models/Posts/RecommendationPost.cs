﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Recommendation post", UseBlocks = false)]
    public class RecommendationPost : Post<RecommendationPost>
    {
        [Region(Title = "Recommendation")]
        public Recommendation Recommendation { get; set; }
    }
}
