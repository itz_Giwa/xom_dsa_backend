﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Contact post", UseBlocks = false)]
    public class ContactPost : Post<ContactPost>
    {
        [Region(Title = "Contact")]
        public Contact Contact { get; set; }
    }
}
