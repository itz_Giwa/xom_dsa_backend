﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Event post", UseBlocks = false)]
    public class EventPost : Post<EventPost>
    {
        [Region(Title = "Event")]
        public Event Event { get; set; }
    }
}
