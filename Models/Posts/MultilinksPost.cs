﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Multilink post", UseBlocks = false)]
    public class MultilinksPost : Post<MultilinksPost>
    {
        [Region(Title = "Multilink")]
        public Multilinks Multilink { get; set; }
    }
}
