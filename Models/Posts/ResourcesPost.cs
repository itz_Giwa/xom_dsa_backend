﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Resources post", UseBlocks = false)]
    public class ResourcesPost : Post<ResourcesPost>
    {
        [Region(Title = "Resource")]
        public Resources Resources { get; set; }
    }
}
