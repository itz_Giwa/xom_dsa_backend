﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Interest post", UseBlocks = false)]
    public class InterestPost : Post<InterestPost>
    {
        [Region(Title = "Interest")]
        public Interest Interest { get; set; }
    }
}
