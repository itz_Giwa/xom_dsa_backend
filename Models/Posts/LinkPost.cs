﻿using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Posts
{
    [PostType(Title = "Link post", UseBlocks = false)]
    public class LinkPost : Post<LinkPost>
    {
        [Region(Title = "Link")]
        public Link Link { get; set; }
    }
}
