using dsa_backend.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Models;

namespace dsa_backend.Models
{
    [PageType(Title = "Blog archive", UseBlocks = false)]
    public class BlogArchive  : ArchivePage<BlogArchive>
    {
        /// <summary>
        /// Gets/sets the archive hero.
        /// </summary>
        [Region]
        public Hero Hero { get; set; }
    }
}