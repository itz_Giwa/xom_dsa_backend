﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Multilinks
    {
        [Field(Title = "Multilinks", Placeholder = "Enter the Multilinks Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField MultilinksCode { get; set; }

        [Field(Title = "Header", Placeholder = "Enter the Header", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Header { get; set; }

        [Field(Title = "Text", Placeholder = "Enter the Text", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Text { get; set; }

        [Field(Title = "Link Text", Placeholder = "Enter the Link Text", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField LinkText { get; set; }

        [Field(Title = "URL", Placeholder = "Enter the URL", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Url { get; set; }
    }
}
