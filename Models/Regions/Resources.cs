﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Resources
    {
        [Field(Title = "Resource Code", Placeholder = "Enter the Resource Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ResourceCode { get; set; }

        [Field(Title = "Resource Type", Placeholder = "Enter the Resource  Type", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ResourceType { get; set; }

        [Field(Title = "Resource Title", Placeholder = "Enter the Resource Title", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ResourceTitle { get; set; }

        [Field(Title = "IntroText", Placeholder = "Enter the Intro Text", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField IntroText { get; set; }

        [Field(Title = "Duration", Placeholder = "Enter the Duration", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Duration { get; set; }

        [Field(Title = "Rating", Placeholder = "Enter the Rating", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Rating { get; set; }
    }
}
