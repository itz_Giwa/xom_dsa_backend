﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class UserInterest
    {
        [Field(Title = "User Intrest", Placeholder = "Enter the User Intrest Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField UserIntrestCode { get; set; }

        [Field(Title = "User", Placeholder = "Enter the User Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField UserCode { get; set; }

        [Field(Title = "Intrest", Placeholder = "Enter the Intrest Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField IntrestCode { get; set; }
    }
}
