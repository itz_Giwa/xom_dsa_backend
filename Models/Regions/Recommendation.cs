﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Recommendation
    {
        [Field(Title = "Recommendation", Placeholder = "Enter the Recommendation Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField RecommendationCode { get; set; }

        [Field(Title = "Option", Placeholder = "Enter the Option Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField OptionCode { get; set; }
        
        [Field(Title = "Contact", Placeholder = "Enter the Contact Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ContactCode { get; set; }

        [Field(Title = "Link", Placeholder = "Enter the Link Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField LinkCode { get; set; }

        [Field(Title = "Multi link", Placeholder = "Enter the Multi link Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField MultiLinkCode { get; set; }

        [Field(Title = "Resource", Placeholder = "Enter the Resource Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ResourceCode { get; set; }

        [Field(Title = "Event", Placeholder = "Enter the Event Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField EventCode { get; set; }

        [Field(Title = "Sequence", Placeholder = "Enter the Recommendation Sequence", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Sequence { get; set; }
    }
}
