﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class User
    {
        [Field(Title = "User Code", Placeholder = "Enter the User Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField UserCode { get; set; }

        [Field(Title = "Name", Placeholder = "Enter the User Name ", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Name { get; set; }
    }
}
