﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Contact
    {
        [Field(Title = "Contact", Placeholder = "Enter the Contact Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ContactCode { get; set; }

        [Field(Title = "Name", Placeholder = "Enter the Contact's Name", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Name { get; set; }

        [Field(Title = "Department", Placeholder = "Enter the Department", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Department { get; set; }

        [Field(Title = "Location", Placeholder = "Enter the Location", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Location { get; set; }
        
        [Field(Title = "Email", Placeholder = "Enter Contact's Email", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Email { get; set; }

        [Field(Title = "IM", Placeholder = "Enter Contact's IM", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField IM { get; set; }

        [Field(Title = "Skype", Placeholder = "Enter Skype username", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Skype { get; set; }

        [Field(Title = "Description", Placeholder = "Enter the Contact's Description", Options = Piranha.Models.FieldOption.HalfWidth)]
        public TextField Description { get; set; }
    }
}
