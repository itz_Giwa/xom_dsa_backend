﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Event
    {
        [Field(Title = "Event Code", Placeholder = "Enter the Event Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField EventCode { get; set; }

        [Field(Title = "Event Name", Placeholder = "Enter the Event Name", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField EventName { get; set; }

        [Field(Title = "IntroText", Placeholder = "Enter the IntroText", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField IntroText { get; set; }

        [Field(Title = "Event Date", Placeholder = "Enter the Event Date", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField EventDate{get; set; }

        [Field(Title = "Location", Placeholder = "Enter the Location", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Location { get; set; }
    }
}
