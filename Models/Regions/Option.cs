﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Option
    {

        [Field(Title = "Option Code", Placeholder = "Enter the Option Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField OptionCode { get; set; }
        
        [Field(Title = "Parent Option", Placeholder = "Enter the Parent Option Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ParentOptionCode { get; set; }

        [Field(Title = "Next Option", Placeholder = "Enter the Next Option Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField NextOptionCode { get; set; }
        
        [Field(Title = "Option Text", Placeholder = "Enter the Option Text", Options = Piranha.Models.FieldOption.HalfWidth)]
        public TextField OptionText { get; set; }


        [Field(Title = "First Option", Options = Piranha.Models.FieldOption.HalfWidth)]
        public CheckBoxField IsFirstOption { get; set; }
    }
}
