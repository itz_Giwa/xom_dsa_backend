﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Interest
    {

        [Field(Title = "Inetrest", Placeholder = "Enter the Interest Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField InterestCode { get; set; }

        [Field(Title = "Name", Placeholder = "Enter the Interest Name ", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField Name { get; set; }
    }
}
