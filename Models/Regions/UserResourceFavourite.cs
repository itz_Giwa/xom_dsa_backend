﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class UserResourceFavourite
    {
        [Field(Title = "User Resource Favourite Code", Placeholder = "Enter the User Resource Favourite  Code", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField UserResourceFavouriteCode { get; set; }

        [Field(Title = "User", Placeholder = "Enter the User Code ", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField UserCode { get; set; }

        [Field(Title = "Recommendation", Placeholder = "Enter the Recommendation Code ", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField RecommendationCode { get; set; }
    }
}
