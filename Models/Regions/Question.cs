﻿using Piranha.AttributeBuilder;
using Piranha.Extend.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dsa_backend.Models.Regions
{
    public class Question
    {
        [Field(Title = "Question Code", Placeholder ="Enter the code", Options=Piranha.Models.FieldOption.HalfWidth)]
        public StringField QuestionCode { get; set; }

        [Field(Title = "Question Text", Placeholder = "Enter the Question")]
        public StringField QuestionText { get; set; }

        [Field(Title = "Help Text", Placeholder = "Enter the Help Text")]
        public StringField HelpText { get; set; }
    }
}
