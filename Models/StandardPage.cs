using Piranha.AttributeBuilder;
using Piranha.Models;

namespace dsa_backend.Models
{
    [PageType(Title = "Standard page")]
    public class StandardPage  : Page<StandardPage>
    {
    }
}