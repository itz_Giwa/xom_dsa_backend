﻿using dsa_backend.Models;
using Microsoft.AspNetCore.Mvc;
using Piranha;
using System;
using System.Threading.Tasks;

namespace dsa_backend.Controllers
{
    public class CmsController : Controller
    {
        private readonly IApi _api;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="api">The current api</param>
        public CmsController(IApi api)
        {
            _api = api;
        }

        /// <summary>
        /// Gets the blog archive with the given id.
        /// </summary>
        /// <param name="id">The unique page id</param>
        /// <param name="year">The optional year</param>
        /// <param name="month">The optional month</param>
        /// <param name="page">The optional page</param>
        /// <param name="category">The optional category</param>
        /// <param name="tag">The optional tag</param>
        [Route("archive")]
        public async Task<IActionResult> Archive(Guid id, int? year = null, int? month = null, int? page = null,
            Guid? category = null, Guid? tag = null)
        {
            var model = await _api.Pages.GetAllAsync();

            return Ok(model);
        }

        /// <summary>
        /// Gets the page with the given id.
        /// </summary>
        /// <param name="id">The unique page id</param>
        [Route("page")]
        public async Task<IActionResult> Page(Guid id)
        {
            var model = await _api.Pages.GetByIdAsync<StandardPage>(id);

            return Ok(model);
        }


        /// <summary>
        /// Gets the page with the given id.
        /// </summary>
        /// <param name="slug">The unique page id</param>
        [Route("pageDetail")]
        public async Task<IActionResult> GetPageDetails(string slug)
        {
            var model = await _api.Pages.GetBySlugAsync(slug);
            return Ok(model);
        }



        /// <summary>
        /// Gets the post with the given id.
        /// </summary>
        /// <param name="id">The unique post id</param>
        [Route("post")]
        public async Task<IActionResult> Post(Guid id)
        {
            var model = await _api.Posts.GetByIdAsync<BlogPost>(id);

            return Ok(model);
        }

        /// <summary>
        /// Gets the startpage with the given id.
        /// </summary>
        /// <param name="id">The unique page id</param>
        [Route("start")]
        public async Task<IActionResult> Start(Guid id)
        {
            var model = await _api.Pages.GetByIdAsync<StartPage>(id);

            return Ok(model);
        }
    }
}
