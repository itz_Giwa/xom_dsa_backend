
using dsa_backend.Models.Posts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Piranha;
using Piranha.Extend.Blocks;
using Syncfusion.XlsIO;
using System;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace dsa_backend.Controllers
{
    /// <summary>
    /// This controller is only used when the project is first started
    /// and no pages has been added to the database. Feel free to remove it.
    /// </summary>
    public class SetupController : Controller
    {
        private readonly IApi _api;

        private const string QuestionArchive = "QuestionArchive";
        private const string OptionArchive = "OptionsArchive";
        private const string ResourceArchive = "ResourcesArchive";
        private const string InterestArchive = "InterestsArchive";
        private const string ContactArchive = "ContactsArchive";
        private const string LinkArchive = "LinksArchive";
        private const string MultilinkArchive = "MultilinksArchive";
        private const string EventArchive = "EventsArchive";
        private const string RecommendationArchive = "RecommendationsArchive";


        private readonly IConfiguration _configuration;

        public SetupController(IApi api, IConfiguration configuration)
        {
            _api = api;
            _configuration = configuration;
        }

        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/seed")]
        public async Task<IActionResult> Seed()
        {

            //load the excel data
            var dataset = LoadExcel();

            // Get the default site
            var site = await _api.Sites.GetDefaultAsync();

            var blogarchive = await _api.Pages.GetAllAsync();


            foreach (var item in blogarchive)
            {
                switch (item.TypeId)
                {

                    case QuestionArchive:
                        await LoadQuestionPost(dataset, item);
                        break;

                    case OptionArchive:
                        await LoadOptionPost(dataset, item);
                        break;

                    case ResourceArchive:
                        await LoadResourcePost(dataset, item);
                        break;

                    case InterestArchive:
                        await LoadInterestPost(dataset, item);
                        break;

                    case ContactArchive:
                        await LoadContactPost(dataset, item);
                        break;

                    case LinkArchive:
                        await LoadLinkPost(dataset, item);
                        break;

                    case EventArchive:
                        await LoadEventPost(dataset, item);
                        break;

                    case RecommendationArchive:
                        await LoadRecommendationPost(dataset, item);
                        break; 
                    case MultilinkArchive:
                    await LoadRecommendationPost(dataset, item);
                    break;

                }

            }


            return Redirect("~/");

        }

        private DataSet LoadExcel()
        {
            var dataset = new DataSet();
            FileStream stream = null;

            using (ExcelEngine excelEngine = new ExcelEngine())
            {


                try
                {
                    //Initialize Application
                    IApplication application = excelEngine.Excel;

                    //Set default application version as Excel 2016
                    application.DefaultVersion = ExcelVersion.Excel2016;

                stream = new FileStream(_configuration.GetSection("path:documentPath").Value, FileMode.Open);
                    IWorkbook workbook = application.Workbooks.Open(stream, ExcelOpenType.Automatic);
                    //IWorksheet worksheet = workbook.Worksheets["];

                    foreach (var item in workbook.Worksheets)
                    {
                        var name = item.Name;
                        var dt = new DataTable(name);

                        //load the data into worksheet
                        dt = item.ExportDataTable(item.UsedRange, ExcelExportDataTableOptions.ComputedFormulaValues | ExcelExportDataTableOptions.ColumnNames);

                        dataset.Tables.Add(dt);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }


            }

            return dataset;
        }

        private async Task LoadQuestionPost(DataSet ds, Piranha.Models.DynamicPage item)
        {
            var dt = ds.Tables["Question"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in dt.Rows)
            {
                var post = QuestionPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Questions";

                post.Question.QuestionCode = row["QuestionCode"].ToString();
                post.Title = row["QuestionCode"].ToString();
                post.Question.QuestionText = row["QuestionText"].ToString();
                post.Question.HelpText = row["HelpText"].ToString();


                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }

        }


        private async Task LoadOptionPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var opt = ds.Tables["Option"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in opt.Rows)
            {
                var post = OptionPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Options";

                post.Title = row["OptionCode"].ToString();
                post.Option.IsFirstOption = row["FirstOption"].ToString() == "TRUE";
                post.Option.OptionCode = row["OptionCode"].ToString();
                post.Option.OptionText = row["OptionText"].ToString();
                post.Option.ParentOptionCode = row["ParentOptionCode"].ToString();
                post.Option.NextOptionCode = row["NextQuestionCode"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }


        private async Task LoadResourcePost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var resource = ds.Tables["Resource"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in resource.Rows)
            {
                var post = ResourcesPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Resource";

                post.Title = row["ResourceCode"].ToString();
                post.Resources.ResourceCode = row["ResourceCode"].ToString();
                post.Resources.ResourceTitle = row["ResourceTitle"].ToString();
                post.Resources.IntroText = row["IntroText"].ToString();
                post.Resources.Duration = row["Duration"].ToString();
                post.Resources.Rating = row["Rating"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }


        private async Task LoadInterestPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var interest = ds.Tables["Interest"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in interest.Rows)
            {
                var post = InterestPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Interest";

                post.Title = row["InterestCode"].ToString();
                post.Interest.InterestCode = row["InterestCode"].ToString();
                post.Interest.Name = row["Name"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }

        private async Task LoadContactPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var contact = ds.Tables["Contact"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in contact.Rows)
            {
                var post = ContactPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Contact";

                post.Title = row["ContactCode"].ToString();
                post.Contact.ContactCode = row["ContactCode"].ToString();
                post.Contact.Name = row["Name"].ToString();
                post.Contact.Department = row["Department"].ToString();
                post.Contact.Location = row["Location"].ToString();
                post.Contact.Description = row["Description"].ToString();
                post.Contact.Email = row["Email"].ToString();
                post.Contact.IM = row["IM"].ToString();
                post.Contact.Skype = row["Skype"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }

        private async Task LoadLinkPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var link = ds.Tables["Link"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in link.Rows)
            {
                var post = LinkPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Link";

                post.Title = row["LinkCode"].ToString();
                post.Link.LinkCode = row["LinkCode"].ToString();
                post.Link.Text = row["Text"].ToString();
                post.Link.LinkText = row["LinkText"].ToString();
                post.Link.Url = row["Url"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }

        private async Task LoadEventPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var events = ds.Tables["Event"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in events.Rows)
            {
                var post = EventPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Event";

                post.Title = row["EventCode"].ToString();
                post.Event.EventCode = row["EventCode"].ToString();
                post.Event.EventName = row["EventName"].ToString();
                post.Event.IntroText = row["IntroText"].ToString();
                post.Event.EventDate = row["EventDate"].ToString();
                post.Event.Location = row["Location"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }

        private async Task LoadMultilinkPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var multilinkpost = ds.Tables["MultiLink"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in multilinkpost.Rows)
            {
                var post = MultilinksPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Multilink";

                post.Title = row["MultilinkCode"].ToString();
                post.Multilink.MultilinksCode = row["MultilinkCode"].ToString();
                post.Multilink.Header = row["Header"].ToString();
                post.Multilink.Text = row["Text"].ToString();
                post.Multilink.Url = row["Url"].ToString();
                post.Multilink.LinkText = row["LinkText"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }


        private async Task LoadRecommendationPost(DataSet ds, Piranha.Models.DynamicPage item)
        {

            var recommendation = ds.Tables["Recommendation"];
            //get all the post
            var allpost = await _api.Posts.GetAllAsync(item.Id);

            //delete all the post
            foreach (var deletepost in allpost)
            {
                await _api.Posts.DeleteAsync(deletepost.Id);
            }

            //load the post
            foreach (DataRow row in recommendation.Rows)
            {
                var post = RecommendationPost.Create(_api);
                var Id = Guid.NewGuid();
                post.BlogId = item.Id;
                post.Category = "Recommendation";

                post.Title = row["RecommendationCode"].ToString();
                post.Recommendation.RecommendationCode = row["RecommendationCode"].ToString();
                post.Recommendation.OptionCode = row["OptionCode"].ToString();
                post.Recommendation.ContactCode = row["contact"].ToString();
                post.Recommendation.LinkCode = row["link"].ToString();
                post.Recommendation.MultiLinkCode = row["multiLink"].ToString();
                post.Recommendation.ResourceCode = row["resources"].ToString();
                post.Recommendation.EventCode = row["event"].ToString();
                post.Recommendation.Sequence = row["sequence"].ToString();

                post.Published = DateTime.Now;
                await _api.Posts.SaveAsync(post);
            }
        }

    }
}
