#pragma checksum "C:\Sandbox\dsa_backend\Views\Cms\Page.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fd1cdad072bb2bd6c99a03854bbe863b1d9081cc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Cms_Page), @"mvc.1.0.view", @"/Views/Cms/Page.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Cms/Page.cshtml", typeof(AspNetCore.Views_Cms_Page))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 3 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha;

#line default
#line hidden
#line 4 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha.Models;

#line default
#line hidden
#line 5 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha.Extend.Blocks;

#line default
#line hidden
#line 7 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using dsa_backend.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd1cdad072bb2bd6c99a03854bbe863b1d9081cc", @"/Views/Cms/Page.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0621400409cbf80d8ee53af833ed1313db3c17ea", @"/Views/_ViewImports.cshtml")]
    public class Views_Cms_Page : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<StandardPage>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(20, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 3 "C:\Sandbox\dsa_backend\Views\Cms\Page.cshtml"
  
    ViewBag.Title = Model.Title;

#line default
#line hidden
            BeginContext(59, 163, true);
            WriteLiteral("\n<div class=\"main\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-sm-10 page-body\">\n                <h1>");
            EndContext();
            BeginContext(223, 11, false);
#line 11 "C:\Sandbox\dsa_backend\Views\Cms\Page.cshtml"
               Write(Model.Title);

#line default
#line hidden
            EndContext();
            BeginContext(234, 23, true);
            WriteLiteral("</h1>\n\n                ");
            EndContext();
            BeginContext(258, 30, false);
#line 13 "C:\Sandbox\dsa_backend\Views\Cms\Page.cshtml"
           Write(Html.DisplayFor(m => m.Blocks));

#line default
#line hidden
            EndContext();
            BeginContext(288, 53, true);
            WriteLiteral("\n            </div>\n        </div>\n    </div>\n</div>\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Piranha.AspNetCore.Services.IApplicationService WebApp { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<StandardPage> Html { get; private set; }
    }
}
#pragma warning restore 1591
