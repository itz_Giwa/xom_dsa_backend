#pragma checksum "C:\Sandbox\dsa_backend\Views\Shared\Partial\_Meta.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4dead4949a56a750f261b0c026284f4b04f1528c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Partial__Meta), @"mvc.1.0.view", @"/Views/Shared/Partial/_Meta.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Partial/_Meta.cshtml", typeof(AspNetCore.Views_Shared_Partial__Meta))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 3 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha;

#line default
#line hidden
#line 4 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha.Models;

#line default
#line hidden
#line 5 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using Piranha.Extend.Blocks;

#line default
#line hidden
#line 7 "C:\Sandbox\dsa_backend\Views\_ViewImports.cshtml"
using dsa_backend.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4dead4949a56a750f261b0c026284f4b04f1528c", @"/Views/Shared/Partial/_Meta.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0621400409cbf80d8ee53af833ed1313db3c17ea", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Partial__Meta : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IMeta>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(13, 22, true);
            WriteLiteral("\n<meta name=\"keywords\"");
            EndContext();
            BeginWriteAttribute("content", " content=\"", 35, "\"", 64, 1);
#line 3 "C:\Sandbox\dsa_backend\Views\Shared\Partial\_Meta.cshtml"
WriteAttributeValue("", 45, Model.MetaKeywords, 45, 19, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(65, 26, true);
            WriteLiteral(">\n<meta name=\"description\"");
            EndContext();
            BeginWriteAttribute("content", " content=\"", 91, "\"", 123, 1);
#line 4 "C:\Sandbox\dsa_backend\Views\Shared\Partial\_Meta.cshtml"
WriteAttributeValue("", 101, Model.MetaDescription, 101, 22, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(124, 64, true);
            WriteLiteral(">\n\n<meta name=\"og:type\" content=\"article\">\n<meta name=\"og:title\"");
            EndContext();
            BeginWriteAttribute("content", " content=\"", 188, "\"", 210, 1);
#line 7 "C:\Sandbox\dsa_backend\Views\Shared\Partial\_Meta.cshtml"
WriteAttributeValue("", 198, Model.Title, 198, 12, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(211, 29, true);
            WriteLiteral(">\n<meta name=\"og:description\"");
            EndContext();
            BeginWriteAttribute("content", " content=\"", 240, "\"", 272, 1);
#line 8 "C:\Sandbox\dsa_backend\Views\Shared\Partial\_Meta.cshtml"
WriteAttributeValue("", 250, Model.MetaDescription, 250, 22, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(273, 2, true);
            WriteLiteral(">\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Piranha.AspNetCore.Services.IApplicationService WebApp { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IMeta> Html { get; private set; }
    }
}
#pragma warning restore 1591
